package at.florianmeltschposter.BHIF_NVS_Projekt1_Poster_Meltsch.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @RequestMapping("/")

    public String home() {
        return "Spring boot is working by Florian Meltsch!";
    }
}